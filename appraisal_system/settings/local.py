from .base import *

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'appraisal_system_live_2018',
        'USER': 'postgres',                     
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '5432',
    },
    '2017': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'appraisal_system_live',
        'USER': 'postgres',                     
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '5432',
    }

}

NEWRELIC_ENV = "local"


STRIPE_PUBLIC_KEY = "pk_test_s40OE6vLgIYRtwfimYJkTBkA"
STRIPE_SECRET_KEY = "sk_test_IYNWFbFH0sMFYft3SMYX0L7U"
