#!/usr/bin/env python

# Python imports.
from __future__ import unicode_literals
import logging
import datetime

# Django imports.
from django.db import models
from appraisal_system import constants
from django.utils.encoding import python_2_unicode_compatible
# Third Party Library imports

# local imports.


# Create your models here.


class TimeStampedModel(models.Model):
    added_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Department(TimeStampedModel):
    dept_name = models.CharField(max_length=255, blank=True, unique=True,verbose_name='Department Name')

    def __str__(self):
        return self.dept_name


class Designation(TimeStampedModel):
    designation_name = models.CharField(max_length=255, blank=True,unique=True, verbose_name='Designation Name')

    def __str__(self):
        return self.designation_name


class Level(TimeStampedModel):
    level_name = models.CharField(max_length=255, blank=True, unique=True,verbose_name='Level Name')

    def __str__(self):
        return self.level_name

class Stack(TimeStampedModel):
    stack_name = models.CharField(max_length=255, blank=True, unique=True,verbose_name='Stack Name')

    def __str__(self):
        return self.stack_name

@python_2_unicode_compatible
class Category(TimeStampedModel):
    category_name = models.TextField(unique=True,verbose_name='Category Name')

    def __str__(self):
        return self.category_name
    class Meta:
        verbose_name_plural = 'Categories'

class KPI(TimeStampedModel):
    kpi_name = models.TextField(unique=True,verbose_name='KPI Name')

    def __str__(self):
        return self.kpi_name
    class Meta:
        verbose_name_plural = 'KPIs'
        
class AssignKPI(TimeStampedModel):
    kpi = models.ForeignKey(KPI, related_name='kpi',verbose_name='KPI Name')
    stack = models.ForeignKey(Stack, related_name='stack')
    level = models.ForeignKey(Level, related_name='level')

    def __str__(self):
        return self.kpi.kpi_name

    class Meta:
        unique_together = ('kpi','stack','level')
        verbose_name_plural = 'Assign KPIs'
        verbose_name = 'Assign KPI'

class Schedule(TimeStampedModel):
    start_date = models.DateField(verbose_name='Start Date')
    end_date = models.DateField(verbose_name='End Date')

class CronInfo(TimeStampedModel):
    last_run_date = models.DateField()
    processing_status = models.IntegerField(default=0,choices=constants.PROCESSING_STATUS)
    cron_type = models.IntegerField(choices=constants.CRON_NAME, default=1)
