# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-28 06:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AssignKPI',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('added_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'Assign KPIs',
            },
        ),
        migrations.CreateModel(
            name='KPI',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('added_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('kpi_name', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterUniqueTogether(
            name='assigncategory',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='assigncategory',
            name='category',
        ),
        migrations.RemoveField(
            model_name='assigncategory',
            name='level',
        ),
        migrations.RemoveField(
            model_name='assigncategory',
            name='stack',
        ),
        migrations.DeleteModel(
            name='AssignCategory',
        ),
        migrations.AddField(
            model_name='assignkpi',
            name='kpi',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='kpi', to='configuration.KPI'),
        ),
        migrations.AddField(
            model_name='assignkpi',
            name='level',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='level', to='configuration.Level'),
        ),
        migrations.AddField(
            model_name='assignkpi',
            name='stack',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='stack', to='configuration.Stack'),
        ),
        migrations.AlterUniqueTogether(
            name='assignkpi',
            unique_together=set([('kpi', 'stack', 'level')]),
        ),
    ]
