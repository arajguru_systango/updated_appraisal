import datetime
from datetime import date
from appraisal_system.utils import *
# import kronos
from employee.models import *
from configuration.models import *
from datetime import date

@kronos.register('0 0 * * *')
def trigger_mail():
    print "Cron is started"
    cron_info = CronInfo.objects.get(cron_type=1)
    if cron_info.last_run_date > date.today():
        if cron_info.processing_status == 1:
            send_email_cron_status(cron_info.processing_status)
        elif cron_info.processing_status == 3:
            send_email_cron_status(cron_info.processing_status)
    try:
        cron_info.processing_status = 1
        cron_info.save()
        schedule = Schedule.objects.all().last()        
        # schedule = Schedule.objects.get(id=1)
        start_date = schedule.start_date
        end_date = schedule.end_date
        if start_date:
            if start_date == date.today():
                print "sending email with active portal status "
                send_email_appraisal_portal_active()
            elif start_date + datetime.timedelta(days=2) == date.today():
                send_email_reminder_mail()
            elif date.today() == start_date + datetime.timedelta(days=3):
                empset = EmployeeProfile.objects.exclude(role__in=('ceo','ceo and rm'))
                for emp in empset:
                    try:
                        reviewerset = Reviewer.objects.filter(employee=emp.id,is_rm_approved=False,is_hr_approved=False)
                        rm = ReportingManager.objects.get(employee=emp.id)
                        send_email_employee_finalize_reviewer(reviewerset, rm.report_manager.email)
                    except:
                        continue                 
            elif start_date + datetime.timedelta(days=5) == date.today():
                send_email_hr_finalize_team()
            elif start_date + datetime.timedelta(days=8) == date.today():
                empset = EmployeeProfile.objects.exclude(role='ceo')
                for emp in empset:
                    try:
                        teamset = Reviewer.objects.filter(reviewer=emp.id,is_rm_approved=True,is_hr_approved=True)
                        send_email_reviewer_fill_feedback(teamset, emp.email)  
                    except:
                        continue
            elif start_date + datetime.timedelta(days=21)== date.today():
                rmset = ReportingManager.objects.distinct('report_manager')
                for rm in rmset:
                    try:
                        empset = ReportingManager.objects.filter(report_manager=rm.report_manager)
                        send_email_rm_fill_feedback(empset, rm.report_manager.email)  
                    except:
                        continue 
            # elif end_date + datetime.timedelta(days=30) == date.today():
            #     empset = EmployeeProfile.objects.exclude(role='ceo')
            #     for emp in empset:
            #         try:
            #             feedback = EmployeeFeedBack.objects.get(employee=emp.id)
            #             send_email_feedback_to_employee(feedback, emp.email) 
            #         except:
            #             continue          
        else:
            cron_info.processing_status = 0
            send_email_cron_status(cron_info.processing_status)
        cron_info.processing_status = 2
        cron_info.last_run_date = date.today()
    except Exception as e:
        cron_info.processing_status = 3
        print str(e)
    cron_info.save()
