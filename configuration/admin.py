from django.contrib import admin
from .models import Department, Designation, Level, Stack, Category, KPI, AssignKPI, Schedule, CronInfo
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from allauth.socialaccount.models import SocialAccount,SocialApp, SocialToken
from allauth.account.models import EmailAddress
from django import forms

# admin.site.register(Group)
admin.site.unregister(Site)
admin.site.unregister(SocialAccount)
admin.site.unregister(SocialApp)
admin.site.unregister(SocialToken)
admin.site.unregister(EmailAddress)

class DepartmentAdmin(admin.ModelAdmin):
    list_display = ['dept_name']
    search_fields = ['dept_name']
admin.site.register(Department, DepartmentAdmin)


class DesignationAdmin(admin.ModelAdmin):
    list_display = ['designation_name']
    search_fields = ['designation_name']
admin.site.register(Designation, DesignationAdmin)


class LevelAdmin(admin.ModelAdmin):
    list_display = ['level_name']
    search_fields = ['level_name']
admin.site.register(Level, LevelAdmin)


class StackAdmin(admin.ModelAdmin):
    list_display = ['stack_name']
    search_fields = ['stack_name']
admin.site.register(Stack, StackAdmin)

from django import forms

class CategoryAdminForm( forms.ModelForm ):
    category_name = forms.CharField( widget=forms.Textarea , label='Category Name')
    class Meta:
        model = Category
        fields = '__all__'

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['category_name']
    search_fields = ['category_name']
    form = CategoryAdminForm
admin.site.register(Category, CategoryAdmin)

class KPIAdmin(admin.ModelAdmin):
    list_display = ['kpi_name']
    search_fields = ['kpi_name']
admin.site.register(KPI, KPIAdmin)

class AssignKPIAdmin(admin.ModelAdmin):
    list_display = ['kpi','stack', 'level']
    list_filter = ['kpi', 'stack', 'level']
admin.site.register(AssignKPI, AssignKPIAdmin)

class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = '__all__'

    def clean(self):
        start_date = self.cleaned_data.get('start_date')
        end_date = self.cleaned_data.get('end_date')
        if end_date <= start_date:
            raise forms.ValidationError("End date cannot be less than or equal to Start date")
        return self.cleaned_data

class ScheduleAdmin(admin.ModelAdmin):
    form = ScheduleForm     
    list_display = ['start_date','end_date']

    def has_add_permission(self, request):
        return False if self.model.objects.count() > 0 else super(ScheduleAdmin, self).has_add_permission(request)
admin.site.register(Schedule, ScheduleAdmin)

admin.site.register(CronInfo)