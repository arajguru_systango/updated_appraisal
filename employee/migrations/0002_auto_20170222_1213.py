# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-22 12:13
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmployeeFeedBack',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('added_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('rm_rating', models.FloatField(blank=True, default=0.0, null=True)),
                ('hr_rating', models.FloatField(blank=True, default=0.0, null=True)),
                ('ceo_rating', models.FloatField(blank=True, default=0.0, null=True)),
                ('rm_feedback', models.CharField(blank=True, max_length=255, null=True)),
                ('hr_feedback', models.CharField(blank=True, max_length=255, null=True)),
                ('ceo_feedback', models.CharField(blank=True, max_length=255, null=True)),
                ('hr_approved', models.BooleanField(default=False)),
                ('ceo_approved', models.BooleanField(default=False)),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, unique=True, verbose_name='employee')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterModelOptions(
            name='feedback',
            options={'verbose_name_plural': 'Feedback'},
        ),
    ]
