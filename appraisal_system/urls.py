"""appraisal_system URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from employee import views as homeview
from allauth.account import views as allauth_view

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^password/change/', homeview.custom_password_change),
    url(r'^$', allauth_view.login, name="account_login"),
    url(r'^', include('allauth.urls')),
    url(r'^home/', include('employee.urls', namespace='employee')),
    url(r'^employee/', include('employee.urls', namespace='employee')),   
]
