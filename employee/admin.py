from django.contrib import admin
from .models import *
from django.contrib.auth.hashers import is_password_usable 


class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['emp_name', 'emp_code', 'email',
                    'emp_level', 'emp_designation', 'emp_department', 'emp_stack',]
    list_filter = ['emp_level', 'emp_designation',
                   'emp_department', 'emp_stack']
    exclude = ['user_permissions','last_login',]
    search_fields = ['emp_name']
    def save_model(self, request, obj, form, change):           
        password = obj.password           
        if not is_password_usable(password):               
            obj.set_password(password)           
        obj.save()

admin.site.register(EmployeeProfile, EmployeeAdmin)

class ReportManagerAdmin(admin.ModelAdmin):
    list_display = ['employee','report_manager', ]
    list_filter = ['report_manager']
admin.site.register(ReportingManager, ReportManagerAdmin)

class ReviewerAdmin(admin.ModelAdmin):
    model = Reviewer
    list_display = ['employee', 'reviewer', 'is_rm_approved','is_hr_approved']
    list_filter = ['is_rm_approved','is_hr_approved']
    list_editable = ['is_rm_approved', 'is_hr_approved',]


    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if request.user.role == 'rm' and db_field.name == "employee":
            rmobjs = ReportingManager.objects.filter(report_manager=request.user.id)
            emps = rmobjs.values('employee')
            kwargs["queryset"] = EmployeeProfile.objects.filter(id__in=emps.values_list('employee', flat=True))
        return super(ReviewerAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
    # def get_readonly_fields(self, request, obj=None):
    #     if request.user.role in ('hr','ceo','hr and rm','ceo and rm'):
    #         if obj:
    #             return ['employee','reviewer']
    #     if request.user.role=='rm':
    #         if obj: # when editing an object
    #             return ['is_hr_approved','employee','reviewer',]
    #     # return self.readonly_fields
    #     return ['employee','reviewer']

    def get_readonly_fields(self, request, obj=None):
        if request.user.role == 'rm':
            return ['is_hr_approved']
        else:
            return []

    def has_module_permission(self, request):
        if not request.user.is_anonymous() and request.user.role in ('hr','rm','ceo','hr and rm','ceo and rm'):
            return True
        return False

    def get_queryset(self, request):
        if request.user.role in ('hr','hr and rm','ceo','ceo and rm'):
            return Reviewer.objects.all()
        if request.user.role == 'rm':
            empset = ReportingManager.objects.filter(report_manager=request.user.id).values_list('employee',flat=True)
            return Reviewer.objects.filter(employee__in=empset)
        return None

    def has_change_permission(self, request, obj=None):
        if request.user.role in ('hr','rm','hr and rm','ceo','ceo and rm'):
            return True
        return False

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        return True

class EmployeeFeedBackAdmin(admin.ModelAdmin):
    list_display = ['employee','report_manager', 'hr_approved', 'ceo_approved']
    list_filter = ['report_manager']
    list_editable = ['hr_approved', 'ceo_approved',]
admin.site.register(EmployeeFeedBack, EmployeeFeedBackAdmin)

admin.site.register(Reviewer, ReviewerAdmin)
admin.site.register(CategoryResult)