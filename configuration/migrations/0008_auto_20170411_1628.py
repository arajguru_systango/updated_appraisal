# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-11 16:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0007_auto_20170318_2046'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='category_name',
            field=models.TextField(unique=True, verbose_name='Category Name'),
        ),
        migrations.AlterField(
            model_name='kpi',
            name='kpi_name',
            field=models.TextField(unique=True, verbose_name='KPI Name'),
        ),
    ]
