from django import forms
from django.db.models import Q
from employee.models import *

from configuration.models import *

class ReviewerForm(forms.Form):
	empset = EmployeeProfile.objects.order_by()
	reviewer = forms.ModelMultipleChoiceField(queryset=empset, required=False, widget=forms.CheckboxSelectMultiple())
	# def __init__(self, user, *args, **kwargs):
	# 	import pdb;pdb.set_trace()
	# 	super(ReviewerForm, self).__init__(*args, **kwargs)
	# 	try:
	# 		rm = ReportingManager.objects.get(employee=user.id)
	# 		self.fields['reviewer'].queryset = EmployeeProfile.objects.exclude(Q(id=rm.report_manager.id)|Q(id=user.id)|Q(emp_name__icontains='admin'))			
	# 	except:
	# 		self.fields['reviewer'].queryset = EmployeeProfile.objects.exclude(Q(id=user.id)|Q(emp_name__icontains='admin'))

class CategoryForm(forms.Form):
	empset = EmployeeProfile.objects.order_by().distinct()
	category = forms.CharField(widget=forms.HiddenInput(), label='category')
	comments = forms.CharField( widget=forms.Textarea(attrs={
						'placeholder': 'Comments','rows': 10,'cols': 80, 
					}),
					required=True)
class UpdateCategoryForm(forms.Form):
	category = forms.CharField(widget=forms.HiddenInput(), label='category')
	comments = forms.CharField(widget=forms.Textarea(attrs={
						'placeholder': 'Comments','rows': 10,'cols': 80, 
					}),
					required=True)	
	
class FeedBackForm(forms.Form):
	rating = forms.FloatField(required=False, max_value=5.0, min_value=0.0)
	feedback = forms.CharField(widget=forms.Textarea(attrs={
						'placeholder': 'Feedback','rows': 10,'cols': 80, 
					}),
					required=True)

class HRFeedBackForm(forms.Form):
	rating = forms.FloatField(required=False, max_value=5.0, min_value=0.0)
	feedback = forms.CharField(widget=forms.Textarea(attrs={
						'placeholder': 'Feedback','rows': 10,'cols': 80, 
					}),
					required=False)
	hr_approved = forms.BooleanField(required=True)

class CEOFeedBackForm(forms.Form):
	rating = forms.FloatField(required=False, max_value=5.0, min_value=0.0)
	feedback = forms.CharField(widget=forms.Textarea(attrs={
						'placeholder': 'Feedback','rows': 10,'cols': 80, 
					}),
					required=False)
	ceo_approved = forms.BooleanField(required=True)


class UpdateFeedBackForm(forms.Form):
	rating = forms.FloatField(required=False,max_value=5.0, min_value=0.0)
	feedback = forms.CharField(widget=forms.Textarea(attrs={
						'placeholder': 'Feedback','rows': 10,'cols': 80, 
					}),
					required=True)

class UpdateHRFeedBackForm(forms.Form):
	rating = forms.FloatField(required=False,max_value=5.0, min_value=0.0)
	feedback = forms.CharField(widget=forms.Textarea(attrs={
						'placeholder': 'Feedback','rows': 10,'cols': 80, 
					}),
					required=False)
	hr_approved = forms.BooleanField(required=True)

class UpdateCEOFeedBackForm(forms.Form):
	rating = forms.FloatField(required=False,max_value=5.0, min_value=0.0)
	feedback = forms.CharField(widget=forms.Textarea(attrs={
						'placeholder': 'Feedback','rows': 10,'cols': 80, 
					}),
					required=False)
	ceo_approved = forms.BooleanField(required=True)