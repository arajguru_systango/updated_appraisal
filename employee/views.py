#!/usr/bin/env python

# Python imports.
import csv, io
import datetime
from django.contrib.auth.decorators import login_required
# Django imports.
from django.shortcuts import render, redirect, HttpResponse
# from django.db.models.loading import get_model
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.http import *
from django.contrib.auth.models import Group
from django.contrib.auth import authenticate, login, logout as django_logout
from django.forms import formset_factory
from django.contrib import messages
# from django.utils import parser
from django.contrib import messages
from django.contrib.messages import get_messages
from django.views.generic import TemplateView
from django.views import View
from django.db.models import Q
# Third Party Library imports

# local imports.
from configuration.models import *
from employee.models import *
from employee.forms import *
import datetime
from appraisal_system.utils import get_disable_date
from django.contrib.auth.decorators import login_required
from allauth.account.views import PasswordChangeView
from .decorators import *
today = datetime.datetime.now()
import logging

logger = logging.getLogger("employee")

class CustomPasswordChangeView(PasswordChangeView):
    @property
    def success_url(self):
        user = self.request.user._wrapped if hasattr(self.request.user,'_wrapped') else None
        if user and user.is_first_login:
            user.is_first_login = False
            user.save()
        self.request.session['pass_msg'] = "Password updated successfully."
        return '/home'

custom_password_change = login_required(CustomPasswordChangeView.as_view())

@login_required(login_url='/login/')
def save_employee_by_csv(request):

    try:
        csvfile = request.FILES['myfile']   
        i_file = csvfile.read().decode('utf-8')
        io_string = io.StringIO(i_file)
        reader = csv.reader(io_string, delimiter=',')
        count = 0
        for row in reader:
            if count > 0:
                emp = EmployeeProfile.objects.get_or_create(emp_code= row[1])[0]
                emp.emp_name = row[0]
                emp.emp_code = row[1]
                        # row[2] = dateutil.parser.parse(row[2]).strftime("%Y-%m-%d")       

                        # emp.emp_doj = row[2]      

                emp.emp_department = Department.objects.get(dept_name=row[2])
                emp.emp_level = Level.objects.get(level_name=row[3])
                emp.emp_designation = Designation.objects.get(designation_name=row[4])
                # emp.emp_technology = Technology.objects.get(tech_name="Python")
                emp.emp_stack = Stack.objects.get(stack_name=row[5])
                emp.email = row[6]
                emp.set_password(row[6])
                if row[7] == "RM":
                    group = Group.objects.get(name='RM Group')
                    emp.groups.add(group)
                    emp.role="rm"
                elif row[7] == "CEO":
                    emp.is_staff = True
                    emp.role="ceo"
                elif row[7] == "HR":
                    emp.is_staff = True
                    emp.role="hr"
                emp.save()
                # return render(request, 'employee/done.html', {'msg': "Incomplete configuration!",})   
            count = count + 1
    except Exception as e:
        return render(request, 'employee/duplicate.html', {'msg':e.__class__.__name__})
    return render(request, 'employee/done.html')


@login_required(login_url='/login/')
def save_rm_by_csv(request):

    try:
        csvfile = request.FILES['myfile']
        i_file = csvfile.read().decode('utf-8')
        io_string = io.StringIO(i_file)
        reader = csv.reader(io_string, delimiter=',')
        count = 0
        for row in reader:
            if count > 0:
                emp = ReportingManager.objects.get_or_create(employee=EmployeeProfile.objects.get(emp_code=row[0]),report_manager=EmployeeProfile.objects.get(emp_code=row[1]))
                emp[0].save()
                # return render(request, 'employee/done.html', {'msg': "Incomplete configuration!",})

            count = count + 1
    except Exception as e:
        return render(request, 'employee/duplicate.html', {'msg':e.__class__.__name__})
    return render(request, 'employee/done.html')

@login_required(login_url='/login/')
def save_kpi_by_csv(request):

    try:
        csvfile = request.FILES['myfile']

        i_file = csvfile.read().decode('utf-8')
        io_string = io.StringIO(i_file)
        reader = csv.reader(io_string, delimiter=',')
        count = 0
        for row in reader:
            if count > 0:
                kpi = KPI.objects.get_or_create(kpi_name__icontains = row[0])[0]
                kpi.kpi_name = row[0]
                kpi.save()
                # return render(request, 'employee/done.html', {'msg': "Incomplete configuration!",})

            count = count + 1
    except Exception as e:
        return render(request, 'employee/duplicate.html', {'msg':e.__class__.__name__})
    return render(request, 'employee/done.html')


@login_required(login_url='/login/')
def save_assignkpi_by_csv(request):

    try:
        
        csvfile = request.FILES['myfile']

        i_file = csvfile.read().decode('utf-8')
        io_string = io.StringIO(i_file)
        reader = csv.reader(io_string, delimiter=',')
        count = 0
        for row in reader:
            if count > 0:
                assignkpi = AssignKPI.objects.get_or_create(kpi = KPI.objects.get(kpi_name=row[0]), 
                    stack = Stack.objects.get(stack_name=row[1]), level = Level.objects.get(level_name = row[2]))[0]

                assignkpi.save()
                # return render(request, 'employee/done.html', {'msg': "Incomplete configuration!",})

            count = count + 1

    except Exception as e:
        return render(request, 'employee/duplicate.html', {'msg':e.__class__.__name__})
    return render(request, 'employee/done.html')

@login_required(login_url='/login/')
def index(request):
    return render(request, 'employee/index.html', {})

@login_required(login_url='/login/')
def home(request):
    logger.info('current user: %s' %request.user)
    if request.user.is_first_login:
        return redirect('/password/change/')
    if request.user.role in ('hr','hr and rm'):
        return HttpResponseRedirect(reverse('employee:hr_home'))
    if request.user.role in ('ceo','ceo and rm'):
        return HttpResponseRedirect(reverse('employee:ceo_home' ))
    if request.user.role == 'rm':
        return HttpResponseRedirect(reverse('employee:report_manager_home'))
    return HttpResponseRedirect(reverse('employee:user_home'))

@login_required(login_url='/login/')
def user_home(request):
    # import pdb;pdb.set_trace();
    logger.info('current user: %s' %request.user)
    try:
        pass_message = request.session.pop('pass_msg') if request.session.has_key('pass_msg') else None
        schedule = Schedule.objects.get(added_date__year=today.year)
        last_date = schedule.start_date + datetime.timedelta(days=3)
        self_form_expired = False
        team_form_active = False
        can_view_feedback = False
        last_date2 = schedule.start_date + datetime.timedelta(days=2)
        last_date3 = schedule.start_date + datetime.timedelta(days=20)
        last_date4 = last_date + datetime.timedelta(days=30)
        if datetime.date.today() >= last_date:
            self_form_expired = True
        if datetime.date.today() >= last_date + datetime.timedelta(days=3) and datetime.date.today() <= last_date + datetime.timedelta(days=14) :
            team_form_active = True 
        if datetime.date.today() >= last_date + datetime.timedelta(days=30):
            can_view_feedback = True    
        # if datetime.today() >= last_date + datetime.timedelta(days=5):
        #   rev_date = True
        request.user.emp_name
    except:
        return render(request, 'employee/error.html')
    return render(request, 'employee/user_home.html', {'user':request.user, 'last_date':last_date, 'last_date2':last_date2,'last_date3':last_date3,'last_date4':last_date4,'self_form_expired':self_form_expired,'team_form_active':team_form_active,'can_view_feedback':can_view_feedback, 'message':pass_message, 'day_left': last_date.strftime("%Y/%m/%d")})

@login_required(login_url='/login/')
def report_manager_home(request):
    logger.info('current user: %s' %request.user)
    try:
        pass_message = request.session.pop('pass_msg') if request.session.has_key('pass_msg') else None
        schedule = Schedule.objects.get(added_date__year=today.year)
        last_date = schedule.start_date + datetime.timedelta(days=3)
        self_form_expired = False
        team_form_active = False
        can_view_feedback = False
        last_date2 = schedule.start_date + datetime.timedelta(days=2)
        last_date3 = schedule.start_date + datetime.timedelta(days=13)
        last_date4 = schedule.start_date + datetime.timedelta(days=5)
        last_date5 = schedule.start_date + datetime.timedelta(days=29)
        if datetime.date.today() >= last_date:
            self_form_expired = True
        if datetime.date.today() >= last_date + datetime.timedelta(days=3) and datetime.date.today() <= last_date + datetime.timedelta(days=14) :
            team_form_active = True 
        if datetime.date.today() >= last_date + datetime.timedelta(days=30):
            can_view_feedback = True        
        request.user.emp_name
    except:
        return render(request, 'employee/error.html')
    return render(request, 'employee/report_manager_home.html', {'user':request.user, 'last_date':last_date, 'last_date2':last_date2,'last_date3':last_date3,'last_date4':last_date4, 'last_date5':last_date5,'self_form_expired':self_form_expired,'team_form_active':team_form_active,'can_view_feedback':can_view_feedback, 'message':pass_message, 'day_left': last_date.strftime("%Y/%m/%d")}) 


@login_required(login_url='/login/')
def hr_home(request):
    logger.info('current user: %s' %request.user)
    try:
        pass_message = request.session.pop('pass_msg') if request.session.has_key('pass_msg') else None
        schedule = Schedule.objects.get(added_date__year=today.year)
        last_date = schedule.start_date + datetime.timedelta(days=3)
        self_form_expired = False
        team_form_active = False
        can_view_feedback = False
        last_date2 = schedule.start_date + datetime.timedelta(days=2)
        last_date3 = schedule.start_date + datetime.timedelta(days=13)
        last_date4 = schedule.start_date + datetime.timedelta(days=5)
        last_date5 = schedule.start_date + datetime.timedelta(days=29)
        if datetime.date.today() >= last_date:
            self_form_expired = True
        if datetime.date.today() >= last_date + datetime.timedelta(days=3) and datetime.date.today() <= last_date + datetime.timedelta(days=14) :
            team_form_active = True 
        if datetime.date.today() >= last_date + datetime.timedelta(days=30):
            can_view_feedback = True
        request.user.emp_name
    except:
        return render(request, 'employee/error.html')
    return render(request, 'employee/hrhome.html', {'user':request.user, 'last_date':last_date, 'last_date2':last_date2,'last_date3':last_date3,'last_date4':last_date4, 'last_date5':last_date5,'self_form_expired':self_form_expired,'team_form_active':team_form_active,'can_view_feedback':can_view_feedback, 'message':pass_message,'day_left': last_date.strftime("%Y/%m/%d") })  

@login_required(login_url='/login/')
def ceo_home(request):
    logger.info('current user: %s' %request.user)
    try:
        pass_message = request.session.pop('pass_msg') if request.session.has_key('pass_msg') else None
        empset = EmployeeProfile.objects.exclude(role__iexact='CEO')
        feedback = EmployeeFeedBack.objects.filter(added_date__year=today.year) 
        schedule = Schedule.objects.get(added_date__year=today.year)
        last_date = schedule.end_date
        self_form_expired = False
        team_form_active = False
        can_view_feedback = False
        last_date2 = last_date + datetime.timedelta(days=3)
        last_date3 = last_date + datetime.timedelta(days=14)
        last_date4 = last_date + datetime.timedelta(days=30)
        if datetime.date.today() >= last_date:
            self_form_expired = True
        if datetime.date.today() >= last_date + datetime.timedelta(days=3) and datetime.date.today() <= last_date + datetime.timedelta(days=14) :
            team_form_active = True 
        if datetime.date.today() >= last_date + datetime.timedelta(days=30):
            can_view_feedback = True
    except:
        return render(request, 'employee/error.html')

    return render(request, 'employee/ceo_home.html', {'empset': empset,'feedback':feedback ,'user':request.user, 'last_date':last_date, 'last_date2':last_date2,'last_date3':last_date3,'last_date4':last_date4, 'self_form_expired':self_form_expired,'team_form_active':team_form_active,'can_view_feedback':can_view_feedback, 'message':pass_message})

@login_required(login_url='/login/')
def view_profile(request):
    try:
        emp = ReportingManager.objects.get(employee=request.user)
    except:
        return render(request, 'employee/user_profile.html', {'user':request.user,'emp':''})
    return render(request, 'employee/user_profile.html', {'user':request.user,'emp':emp})

@login_required(login_url='/login/')
@pass_decor('define_reviewers')
def define_reviewers(request, emp_id):
    logger.info('current user: %s employee_id: %s' %(request.user, emp_id))
    user = request.user
    try:
        rm = ReportingManager.objects.get(employee=user.id)
        empset = EmployeeProfile.objects.exclude(Q(id=rm.report_manager.id)|Q(id=user.id)|Q(emp_name__icontains='admin'))   
    except:
        empset = EmployeeProfile.objects.exclude(Q(id=user.id)|Q(emp_name__icontains='admin'))
    if request.method == 'GET': 
        ReviewerForm.base_fields['reviewer'] = forms.ModelMultipleChoiceField(queryset= empset) 
        form = ReviewerForm()
    else:       
        form = ReviewerForm(request.POST)
        if form.is_valid():
            reviewer_data = form.cleaned_data.get('reviewer')
            # if len(reviewer_data) >= 3 and len(reviewer_data) <= 8:
            if len(reviewer_data) <= 8:
                reviewerset = Reviewer.objects.filter(employee=user.id)
                count = len(reviewerset)
                total_count = len(reviewer_data) + count
                if not(total_count >= 3 and total_count <= 8):
                    return render(request, 'employee/reviewer.html', {'msg': "Min 3 and Max 8 reviewer can be selected",})
                for reviewer in reviewer_data:
                    obj = Reviewer()
                    obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                    obj.reviewer = reviewer
                    obj.is_approved = False

                    # reviewerset = Reviewer.objects.filter(employee=user.id)
                    # count = len(reviewerset)
                    # if count > 8:
                    #   return render(request, 'employee/reviewer.html', {'msg': "Min 3 and Max 8 reviewer can be selected",})

                    try:
                        obj.save()
                    except:
                        return render(request, 'employee/reviewer.html', {'msg': "You have already added one of the reviewers!",})  
            else:
                return render(request, 'employee/reviewer.html', {'msg': "Min 3 and Max 8 reviewer should be selected",})

        return HttpResponseRedirect(reverse('employee:user_home'))

    return render(request, 'employee/reviewer.html', {'form': form,'emp_id':emp_id,})

@login_required(login_url='/login/')
@pass_decor('view_reviewers')
def view_reviewers(request, emp_id):
    logger.info('current user: %s employee_id: %s' %(request.user, emp_id))
    empset = Reviewer.objects.filter(employee=emp_id,added_date__year=today.year)   
    if empset.exists():
        return render(request, 'employee/view_reviewer.html', {'empset': empset, })
    return render(request, 'employee/view_reviewer.html', {'msg': "You have not added any reviewers!",})    

@login_required(login_url='/login/')
def delete_reviewer(request, reviewer_id):
    emp_id=request.user.id
    logger.info('current user: %s employee_id: %s' %(request.user, emp_id))
    # import pdb;pdb.set_trace()
    
    obj = Reviewer.objects.get(employee=emp_id,reviewer=reviewer_id,added_date__year=today.year)
    if Reviewer.objects.filter(employee=emp_id,added_date__year=today.year).count() > 3:
        obj.delete()
    else:
        return render(request, 'employee/view_reviewer.html', {'msg': 'Cannot delete reviewer, minimum 3 reviewers are required.', })
    return HttpResponseRedirect(reverse('employee:view_reviewers', args=(emp_id,)))

# @login_required(login_url='/login/')
# def fill_category_form(request, emp_id):

#   emp = get_object_or_404(EmployeeProfile, pk=emp_id)
#   stack = emp.emp_stack
#   level = emp.emp_level
#   kpiset = AssignKPI.objects.filter(level=level,stack=stack)
#   catset = Category.objects.all()

#   CategoryFormSet = formset_factory(CategoryForm, extra=0)
#   if request.method == 'GET':
#       formset = CategoryFormSet(initial=[{'category': "%s" % c.category_name} for c in catset])
#   else:
#       formset = CategoryFormSet(request.POST)

#       if formset.is_valid():
#           for form in formset:
#               obj = CategoryResult()
#               obj.category = Category.objects.get(category_name=(form.cleaned_data.get('category')))
#               obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
#               obj.comments = form.cleaned_data.get('comments')
#               obj.filled_by = request.user
#               try:
#                   obj.save()
#               except:
#                   return render(request, 'employee/kpiform.html', {'msg': "You have already filled the form!",'emp_id':emp_id,})
#           messages.info(request, 'Your form has been submitted')
#           # return render(request, 'employee/kpiform.html', {'msg': "done",})

#   return render(request, 'employee/kpiform.html', {'formset': formset, 'emp_id':emp_id,'emp':emp,'kpiset':kpiset})

# @login_required(login_url='/login/')
# def update_category_form(request, emp_id):
#   commset = CategoryResult.objects.filter(employee=emp_id,filled_by=request.user,added_date__year=today.year)
#   emp = get_object_or_404(EmployeeProfile, pk=emp_id)
#   stack = emp.emp_stack
#   level = emp.emp_level
#   kpiset = AssignKPI.objects.filter(level=level,stack=stack) 
#   CategoryFormSet = formset_factory(UpdateCategoryForm, extra=0)
#   if request.method == 'GET':
#       formset = CategoryFormSet(initial=[{'category': "%s" % c.category,'comments' : "%s" % c.comments} for c in commset ])
#   else:
#       formset = CategoryFormSet(request.POST)
#       # import pdb;pdb.set_trace()

#       if formset.is_valid():
#           for form in formset:
#               # kpi =
#               obj = CategoryResult.objects.get(employee=emp_id,filled_by=request.user,category=Category.objects.get(category_name=(form.cleaned_data.get('category'))))
#               obj.category = Category.objects.get(category_name=(form.cleaned_data.get('category')))
#               obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
#               obj.comments = form.cleaned_data.get('comments')
#               obj.filled_by = request.user
#               try:
#                   obj.save()
#               except:
#                   return render(request, 'employee/kpiformupdate.html', {'msg': "Duplicate entry!",})
#           # return render(request, 'employee/kpiformupdate.html', {'msg': "Form Updated!",})
#           messages.info(request,'Form Updated!')

#   return render(request, 'employee/kpiformupdate.html', {'formset': formset, 'emp_id':emp_id,'kpiset':kpiset})

@login_required(login_url='/login/')
@pass_decor('save_category_form')
@restrict_user('save_category_form')
def save_category_form(request, emp_id):
    logger.info('current user: %s employee_id: %s' %(request.user, emp_id))
    employee = EmployeeProfile.objects.get(pk=emp_id)
    commset_last = CategoryResult.objects.using('2017').filter(employee__email=employee.email,filled_by__email=request.user.email)
    commset = CategoryResult.objects.filter(employee=emp_id,filled_by=request.user,added_date__year=today.year)
    catset = Category.objects.all()
    emp = get_object_or_404(EmployeeProfile, pk=emp_id)
    stack = emp.emp_stack
    level = emp.emp_level
    kpiset = AssignKPI.objects.filter(level=level,stack=stack) 
    CategoryFormSet = formset_factory(UpdateCategoryForm, extra=0)
    if request.method == 'GET':
        if commset:
            formset = CategoryFormSet(initial=[{'category': "%s" % c.category,'comments' : "%s" % c.comments} for c in commset ])
        else:
            formset = CategoryFormSet(initial=[{'category': "%s" % c.category_name} for c in catset])
    else:
        formset = CategoryFormSet(request.POST)
        # import pdb;pdb.set_trace()

        if formset.is_valid():
            for form in formset:
                # kpi =
                try:
                    obj = CategoryResult.objects.get(employee=emp_id,filled_by=request.user,category=Category.objects.get(category_name=(form.cleaned_data.get('category'))))
                except:
                    obj = CategoryResult()
                obj.category = Category.objects.get(category_name=(form.cleaned_data.get('category')))
                obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                obj.comments = form.cleaned_data.get('comments')
                obj.filled_by = request.user
                try:
                    obj.save()
                except:
                    return render(request, 'employee/kpiformsave.html', {'msg': "Duplicate entry!",})
            return render(request, 'employee/kpiformsave.html', {'msg':'Form Saved!',})
            # messages.info(request,'Form Saved!')

    return render(request, 'employee/kpiformsave.html', {'formset': formset, 'emp_id':emp_id,'kpiset':kpiset, 'commset_last':[]})

def save_comments_form(data,user,emp_id):
    # import pdb;pdb.set_trace()
    data.pop('csrfmiddlewaretoken') if data.has_key('csrfmiddlewaretoken') else None
    data.pop('rating') if data.has_key('rating') else None
    data.pop('feedback') if data.has_key('feedback') else None
    data.pop('ceo_approved') if data.has_key('ceo_approved')else None
    data.pop('hr_approved') if data.has_key('hr_approved')else None
    if data:
        for key,value in data.iteritems():
            try:
                obj = CategoryResult.objects.get(employee=emp_id,filled_by=user,category_id = key)
            except:
                obj = CategoryResult()
            obj.category = Category.objects.get(pk=key)
            obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
            obj.comments = value.strip()
            obj.filled_by = user
            obj.save()

    return data

@login_required(login_url='/login/')
def save_category_form_new(request, emp_id):
    logger.info('current user: %s employee_id: %s' %(request.user, emp_id))
    if request.method == 'POST':
        data = request.POST.dict()
        data.pop('csrfmiddlewaretoken') if data.has_key('csrfmiddlewaretoken') else None
        if data:
            for key,value in data.iteritems():
                try:
                    obj = CategoryResult.objects.get(employee=emp_id,filled_by=request.user,category_id = key)
                except:
                    obj = CategoryResult()
                obj.category = Category.objects.get(pk=key)
                obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                obj.comments = value.trim()
                obj.filled_by = request.user
                obj.save()
            return render(request, 'employee/kpiformsave.html', {'msg':'Form Saved!',}) 
    return render(request, 'employee/kpiformsave.html', {'msg':'Only post method is saved.',})  

@login_required(login_url='/login/')
@pass_decor('view_feedback')
def view_feedback(request, emp_id):
    logger.info('current user: %s employee_id: %s' %(request.user, emp_id))
    try:
        catset = CategoryResult.objects.filter(employee=emp_id, filled_by__role__in=['ceo', 'ceo and rm'])
        if not catset:
            catset = CategoryResult.objects.filter(employee=emp_id, filled_by__role='rm')
        feedback = EmployeeFeedBack.objects.filter(employee=emp_id,hr_approved=True,ceo_approved=True,added_date__year=today.year)
    except:
        return render(request, 'employee/feedback.html', {'msg': "No feedback yet!",})
    return render(request, 'employee/feedback.html', {'feedback': feedback.first(), 'summary':None })

@login_required(login_url="/login/")
def get_rating(request):
    if request.user.role in ["hr", "hr and rm"]:
        year = request.GET.get('year', "")
        logger.info('current user: %s' %(request.user))
        if year:
            feedbacks = EmployeeFeedBack.objects.using("2017").all()
            response = HttpResponse(content_type='text/csv')
            csv_name = "rating_2017.csv"
            response['Content-Disposition'] = 'attachment;filename = "%s"'%csv_name
            writer = csv.writer(response)
            writer.writerow(["Name ", "RM Rating", "HR Rating", "CEO Ratings"])
            for feedback in feedbacks:
                if EmployeeProfile.objects.filter(email=feedback.employee.email):
                    data = []
                    data.append(feedback.employee.get_full_name())
                    data.append(feedback.rm_rating)
                    data.append(feedback.hr_rating)
                    data.append(feedback.ceo_rating)
                    writer.writerow(data)
            return response
        else:
            feedbacks = EmployeeFeedBack.objects.all()
            response = HttpResponse(content_type='text/csv')
            csv_name = "rating.csv"
            response['Content-Disposition'] = 'attachment;filename = "%s"'%csv_name
            writer = csv.writer(response)
            writer.writerow(["Name ", "RM Rating", "HR Rating", "CEO Ratings"])
            for feedback in feedbacks:
                data = []
                data.append(feedback.employee.get_full_name())
                data.append(feedback.rm_rating)
                data.append(feedback.hr_rating)
                data.append(feedback.ceo_rating)
                writer.writerow(data)
            return response
    else :
        return render(request, 'employee/unauthorized.html')       

@login_required(login_url='/login/')
@pass_decor('team_view')
@restrict_user('team')
def team(request, emp_id):
    logger.info('current user: %s employee_id: %s' %(request.user, emp_id))
    empset = Reviewer.objects.filter(reviewer=emp_id,is_rm_approved=True,is_hr_approved=True,added_date__year=today.year)
    categoryresult = CategoryResult.objects.filter(filled_by=emp_id,added_date__year=today.year).distinct('employee')
    if empset.exists():
        return render(request, 'employee/team.html', {'empset':empset,'categoryresult':categoryresult,})
    return render(request, 'employee/team.html', {'msg': "No members yet!",})


@login_required(login_url='/login/')
@restrict_user('employees_under_rm')
def employees_under_rm(request, emp_id):
    logger.info('current user: %s employee_id: %s' %(request.user, emp_id))
    empset = ReportingManager.objects.filter(report_manager=emp_id,added_date__year=today.year)
    if not empset:
        return render(request, 'employee/employee_under_rm.html', {'msg': "No members with you!",})
    categoryresult = CategoryResult.objects.filter(filled_by=emp_id,added_date__year=today.year).distinct('employee')
    feedback = EmployeeFeedBack.objects.filter(added_date__year=today.year)
    schedule = Schedule.objects.get(added_date__year=today.year)
    #need to uncomment on live
    # active_date = schedule.start_date + datetime.timedelta(days=3)    
    active_date = schedule.start_date   
    finalize_reviewer_active = False
    employee_under_rm_form_active =False
    feedback_active = False
    if datetime.date.today() >= active_date and datetime.date.today() <= active_date + datetime.timedelta(days=6):
        finalize_reviewer_active = True
    # if datetime.date.today() >= last_date + datetime.timedelta(days=3) and datetime.date.today() < last_date + datetime.timedelta(days=14) :
    #   employee_under_rm_form_active = True
    # if datetime.date.today() >= last_date + datetime.timedelta(days=14) and datetime.date.today() < last_date + datetime.timedelta(days=30):
    #   feedback_active = True
    return render(request, 'employee/employee_under_rm.html', {'empset': empset, 'categoryresult':categoryresult, 'feedback':feedback,'finalize_reviewer_active':finalize_reviewer_active,'employee_under_rm_form_active':employee_under_rm_form_active,'feedback_active':feedback_active})

@login_required(login_url='/login/')
def view_all_employees(request, emp_id):    
    empset = EmployeeProfile.objects.exclude(Q(role='ceo')|Q(role='ceo and rm')|Q(id=request.user.id)|Q(emp_name__icontains='admin'))
    feedback = EmployeeFeedBack.objects.filter(added_date__year=today.year)
    schedule = Schedule.objects.get(added_date__year=today.year)
    last_date = schedule.start_date   
    finalize_reviewer_active = False
    feedback_active = False
    # import pdb;pdb.set_trace()
    if datetime.date.today() >= last_date and datetime.date.today() < last_date + datetime.timedelta(days=8):
        finalize_reviewer_active = True
    if datetime.date.today() >= last_date + datetime.timedelta(days=14) and datetime.date.today() < last_date + datetime.timedelta(days=30):
        feedback_active = True
    return render(request, 'employee/viewallemployees.html', {'empset': empset,'feedback':feedback,'finalize_reviewer_active':finalize_reviewer_active, 'feedback_active':feedback_active})


@login_required(login_url='/login/')    
@pass_decor('view_competency')
@restrict_user('view_competency')
def view_competency(request, emp_id):
    # import pdb;pdb.set_trace()
    data = request.POST.dict()
    current_user = request.user
    # import pdb;pdb.set_trace()
    if request.user.role in ('rm','ceo', 'hr'):
        returned_data = save_comments_form(data,current_user,emp_id)
    logger.info('current user: %s employee_id: %s' %(request.user, emp_id)) 
    is_rm = False
    catset = CategoryResult.objects.filter(employee=emp_id).distinct('category')
    catset_self = CategoryResult.objects.filter(employee=emp_id,filled_by=request.user).distinct('category')
    name = EmployeeProfile.objects.only('emp_name').get(pk=emp_id).emp_name
    emp = get_object_or_404(EmployeeProfile, pk=emp_id)
    stack = emp.emp_stack
    level = emp.emp_level
    kpiset = AssignKPI.objects.filter(level=level,stack=stack) 
    if request.user.role == 'ceo and rm':
        rm = ReportingManager.objects.get(employee=emp_id)
        if request.user == rm.report_manager:
            if request.method == 'GET':
                result = EmployeeFeedBack.objects.filter(employee=emp_id, added_date__year=today.year)
                if result :
                    form = UpdateCEOFeedBackForm(initial = {'rating':result.first().ceo_rating,'feedback':result.first().ceo_feedback,'hr_approved':result.first().ceo_approved})
                else:
                    form = CEOFeedBackForm()
                # form = CEOFeedBackForm()
                is_rm = True
            else:
                form = CEOFeedBackForm(request.POST)
                if form.is_valid():
                    obj = None
                    try:
                        obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                    except:
                        obj = EmployeeFeedBack()
                    obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                    obj.rm_rating = form.cleaned_data.get('rating')
                    obj.rm_feedback = form.cleaned_data.get('feedback')
                    obj.ceo_rating = form.cleaned_data.get('rating')
                    obj.ceo_feedback = form.cleaned_data.get('feedback')
                    obj.ceo_approved = form.cleaned_data.get('ceo_approved')
                    obj.report_manager = request.user
                    obj.ceo = request.user
                    try:
                        obj.save()
                    except:
                        return render(request, 'employee/categoryset.html', {'msg': "This feedback has already been given!", 'emp_id':emp_id,})
                    return render(request, 'employee/categoryset.html', {'msg': "Your feedback has been submitted",'emp_id':emp_id})
        else:
            if request.method == 'GET':
                result = EmployeeFeedBack.objects.filter(employee=emp_id, added_date__year=today.year)
                if result :
                    form = UpdateCEOFeedBackForm(initial = {'rating':result.first().ceo_rating,'feedback':result.first().ceo_feedback,'ceo_approved':result.first().ceo_approved})
                else:
                    form = CEOFeedBackForm()
                is_rm = True
            else:
                form = CEOFeedBackForm(request.POST)
                if form.is_valid():
                    try: 
                        obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                    except:
                        return render(request, 'employee/categoryset.html', {'msg2': "You cannot give this feedback till RM has given his/her feedback",'form':form,'name':name,'catset':catset,'emp_id':emp_id,'kpiset':kpiset})
                    if obj.ceo_rating and obj.ceo_feedback:
                        return render(request, 'employee/categoryset.html', {'msg': "This feedback has already been given!",'emp_id':emp_id,})
                    obj.ceo_rating = form.cleaned_data.get('rating')
                    obj.ceo_feedback = form.cleaned_data.get('feedback')
                    obj.ceo_approved = form.cleaned_data.get('ceo_approved')
                    obj.ceo = request.user
                    obj.save()
                    return render(request, 'employee/categoryset.html', {'msg': "Your feedback has been submitted",'emp_id':emp_id,})
            try:
                feedback = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                return render(request, 'employee/categoryset.html', {'feedback':feedback,'form': form,'name':name,'catset':catset,'emp_id':emp_id,'kpiset':kpiset})
            except:
                return render(request, 'employee/categoryset.html', {'msg2': "No feedback given by RM yet!",'form':form,'name':name,'catset':catset,'emp_id':emp_id,'kpiset':kpiset})   

    elif request.user.role == 'hr and rm':
        rm = ReportingManager.objects.get(employee=emp_id)
        if request.user == rm.report_manager:
            if request.method == 'GET':
                result = EmployeeFeedBack.objects.filter(employee=emp_id, added_date__year=today.year)
                if result :
                    form = UpdateHRFeedBackForm(initial = {'rating':result.first().hr_rating,'feedback':result.first().hr_feedback,'hr_approved':result.first().hr_approved})
                else:
                    form = HRFeedBackForm()
                is_rm = True
            else:
                form = HRFeedBackForm(request.POST)
                # import pdb;pdb.set_trace()
                if form.is_valid():
                    obj = None
                    try:
                        obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                    except:
                        obj = EmployeeFeedBack()
                    obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                    obj.rm_rating = form.cleaned_data.get('rating')
                    obj.rm_feedback = form.cleaned_data.get('feedback')
                    obj.hr_rating = form.cleaned_data.get('rating')
                    obj.hr_feedback = form.cleaned_data.get('feedback')
                    obj.hr_approved = form.cleaned_data.get('hr_approved')
                    obj.report_manager = request.user
                    obj.hr = request.user
                    try:
                        obj.save()
                    except:
                        return render(request, 'employee/categoryset.html', {'msg': "This feedback has already been given!", 'emp_id':emp_id,})
                    return render(request, 'employee/categoryset.html', {'msg': "Your feedback has been submitted",'emp_id':emp_id})
        else:
            if request.method == 'GET':
                result = EmployeeFeedBack.objects.filter(employee=emp_id, added_date__year=today.year)
                if result :
                    form = UpdateHRFeedBackForm(initial = {'rating':result.first().hr_rating,'feedback':result.first().hr_feedback,'hr_approved':result.first().hr_approved})
                else:
                    form = HRFeedBackForm()
                is_rm = True
            else:
                form = HRFeedBackForm(request.POST)
                if form.is_valid():
                    try: 
                        obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                    except:
                        return render(request, 'employee/categoryset.html', {'msg2': "You cannot give this feedback till RM has given his/her feedback",'form':form,'name':name,'catset':catset,'emp_id':emp_id,'kpiset':kpiset})
                    if obj.hr_rating and obj.hr_feedback:
                        return render(request, 'employee/categoryset.html', {'msg': "This feedback has already been given!",'emp_id':emp_id,})
                    obj.hr_rating = form.cleaned_data.get('rating')
                    obj.hr_feedback = form.cleaned_data.get('feedback')
                    obj.hr_approved = form.cleaned_data.get('hr_approved')
                    obj.hr = request.user
                    obj.save()
                    return render(request, 'employee/categoryset.html', {'msg': "Your feedback has been submitted",'emp_id':emp_id,})
            try:
                feedback = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                return render(request, 'employee/categoryset.html', {'feedback':feedback,'form': form,'name':name,'catset':catset,'emp_id':emp_id,'kpiset':kpiset})
            except:
                return render(request, 'employee/categoryset.html', {'msg2': "No feedback given by RM yet!",'form':form,'name':name,'catset':catset,'emp_id':emp_id,'kpiset':kpiset})                   


    # elif request.user.role == 'hr':
    #   if request.method == 'GET':
            
    #       form = HRFeedBackForm()
    #   else:
    #       form = HRFeedBackForm(request.POST)     
    #       if form.is_valid(): 
    #           # import pdb;pdb.set_trace()
    #           try: 
    #               obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
    #           except:
    #               obj = EmployeeFeedBack()
    #           obj.hr_rating = form.cleaned_data.get('rating')
    #           obj.hr_feedback = form.cleaned_data.get('feedback')
    #           obj.hr_approved = form.cleaned_data.get('hr_approved')
    #           obj.hr = request.user
    #           obj.save()
    #           return render(request, 'employee/categoryset.html', {'msg': "Your feedback has been submitted",'emp_id':emp_id, 'user_id':request.user.id})
    #   try:
    #       feedback = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
    #       return render(request, 'employee/categoryset.html', {'feedback':feedback,'form': form,'name':name,'catset':catset,'emp_id':emp_id,'kpiset':kpiset})
    #   except:
    #       return render(request, 'employee/categoryset.html', {'msg2': "No feedback given by RM yet!",'form':form,'name':name,'catset':catset,'emp_id':emp_id,'kpiset':kpiset})


    elif request.user.role=='hr' :
        if request.method == 'GET':
            # catset_self = CategoryResult.objects.filter(employee=emp_id,filled_by=request.user).distinct('category')

            # user_id = request.user.id
            # if not catset_self.exists():
            rm_user = ReportingManager.objects.filter(employee_id = emp_id)
            if rm_user:

                user_id = rm_user.first().report_manager.id
                catset_self = CategoryResult.objects.filter(employee=emp_id, filled_by=rm_user.first().report_manager).distinct('category')
                if not catset_self.exists():
                    catset_self = catset
            try:
                result =EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
            except:
                result = EmployeeFeedBack()
            form = HRFeedBackForm(initial= {'rating':result.hr_rating,'feedback':result.hr_feedback,'hr_approved':result.hr_approved})
            is_rm = True
        else:
            form = HRFeedBackForm(request.POST)     
            if form.is_valid():  
                try:
                    obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year) 
                except:
                    obj = EmployeeFeedBack()
                obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                obj.hr_rating = form.cleaned_data.get('rating')
                obj.hr_feedback = form.cleaned_data.get('feedback')
                obj.hr_approved = form.cleaned_data.get('hr_approved')
                obj.hr = request.user
                obj.save()
                return render(request, 'employee/categoryset.html', {'msg': "Your feedback has been submitted",'emp_id':emp_id})
        try:
            feedback = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
            return render(request, 'employee/categoryset.html', {'feedback':feedback,'form': form,'name':name,'catset':catset_self,'emp_id':emp_id,'kpiset':kpiset, 'is_rm':is_rm })
        except:
            return render(request, 'employee/categoryset.html', {'msg2': "No feedbacks given yet!",'form':form,'name':name,'catset':catset_self,'emp_id':emp_id,'kpiset':kpiset,'is_rm':is_rm})     

    elif request.user.role == 'ceo':
        if request.method == 'GET':
            catset_self = CategoryResult.objects.filter(employee=emp_id,filled_by=request.user).distinct('category')
            user_id = request.user.id
            if not catset_self.exists():
                rm_user = ReportingManager.objects.filter(employee_id = emp_id)
                if rm_user:

                    user_id = rm_user.first().report_manager.id
                    catset_self = CategoryResult.objects.filter(employee=emp_id, filled_by=rm_user.first().report_manager).distinct('category')
                    if not catset_self.exists():
                        catset_self = catset
                if rm_user.first().report_manager == request.user:
                    catset_self = catset
            try:
                result =EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
            except:
                result = EmployeeFeedBack()
            form = CEOFeedBackForm(initial= {'rating':result.ceo_rating,'feedback':result.ceo_feedback,'ceo_approved':result.ceo_approved})
            is_rm = True
        else:
            form = CEOFeedBackForm(request.POST)        
            if form.is_valid():  
                try:
                    obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year) 
                except:
                    obj = EmployeeFeedBack()
                obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                obj.ceo_rating = form.cleaned_data.get('rating')
                obj.ceo_feedback = form.cleaned_data.get('feedback')
                obj.ceo_approved = form.cleaned_data.get('ceo_approved')
                obj.ceo = request.user
                rm_user = ReportingManager.objects.get(employee_id = emp_id)
                if request.user == rm_user.report_manager:
                    obj.rm_rating = form.cleaned_data.get('rating')
                    obj.rm_feedback = form.cleaned_data.get('feedback')
                    obj.report_manager = request.user
                obj.save()
                return render(request, 'employee/categoryset.html', {'msg': "Your feedback has been submitted",'emp_id':emp_id})
        try:
            feedback = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
            return render(request, 'employee/categoryset.html', {'feedback':feedback,'form': form,'name':name,'catset':catset_self,'emp_id':emp_id,'kpiset':kpiset, 'is_rm':is_rm, 'user_id': user_id })
        except:
            return render(request, 'employee/categoryset.html', {'msg2': "No feedbacks given yet!",'form':form,'name':name,'catset':catset_self,'emp_id':emp_id,'kpiset':kpiset,'is_rm':is_rm, 'user_id': user_id})

    elif request.user.role=='rm' :
        try:
            result = EmployeeFeedBack.objects.get(employee=emp_id, added_date__year=today.year)
        except:
            result = EmployeeFeedBack()
        if request.method == 'GET':
            form = FeedBackForm(initial= {'rating':result.rm_rating,'feedback':result.rm_feedback,})
            is_rm = True
        else:
            form = FeedBackForm(request.POST)
            if form.is_valid(): 
                try:
                    obj = EmployeeFeedBack.objects.get (employee=emp_id, added_date__year=today.year)
                except:
                    obj = EmployeeFeedBack()                    
                obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id) 
                obj.rm_rating = form.cleaned_data.get('rating')
                obj.rm_feedback = form.cleaned_data.get('feedback')
                obj.report_manager = request.user
                try:
                    obj.save()
                except:
                    return render(request, 'employee/categoryset.html', {'msg': "You have already given this feedback!", 'emp_id':emp_id,})
                return render(request, 'employee/categoryset.html', {'msg': "Your feedback has been submitted",'emp_id':emp_id, 'user_id':request.user.id}) 
    return render(request, 'employee/categoryset.html', {'form': form,'name':name,'catset':catset_self if catset_self.exists() else catset ,'emp_id':emp_id,'kpiset':kpiset, 'is_rm': is_rm, 'user_id':request.user.id, })  

@login_required(login_url='/login/')
@restrict_user('view_competency_result')
def view_competency_result(request, emp_id, category_id):
    logger.info('current user: %s employee_id: %s' %(request.user, emp_id))
    resultset_self = CategoryResult.objects.filter(employee=emp_id,filled_by=emp_id,category=category_id,added_date__year=today.year)
    resultset = CategoryResult.objects.filter(employee=emp_id,category=category_id,added_date__year=today.year).exclude(filled_by=emp_id)
    return render(request, 'employee/categoryresult.html', {'resultset_self':resultset_self,'resultset':resultset,'category_id':category_id, 'emp_id':emp_id})  

@login_required(login_url="/login")
@restrict_user('view_competency_result')
def view_competency_result_previous(request, emp_id, category_id):
    logger.info('current user: %s employee_id: %s' %(request.user, emp_id))
    employee = EmployeeProfile.objects.get(pk=emp_id)
    category = Category.objects.get(pk=category_id)
    resultset = CategoryResult.objects.using('2017').filter(employee__email=employee.email,category__category_name=category.category_name)
    return render(request, 'employee/categoryresult.html', {'resultset':resultset,'category_id':category_id})   


@login_required(login_url='/login/')
def update_feedback(request, emp_id):
    logger.info('current user: %s employee_id: %s' %(request.user, emp_id))
    name = EmployeeProfile.objects.only('emp_name').get(pk=emp_id).emp_name
    try:
        result = EmployeeFeedBack.objects.get(employee=emp_id, added_date__year=today.year)
    except:
        return render(request, 'employee/updatefeedback.html', {'msg': "You have not given a feedback yet!",})

    if request.user.role == 'ceo and rm':
        rm = ReportingManager.objects.get(employee=emp_id)
        if request.user == rm.report_manager:
            if request.method == 'GET':
                form = UpdateCEOFeedBackForm(initial = {'rating':result.rm_rating,'feedback':result.rm_feedback, 'ceo_approved':result.ceo_approved})
            else:
                form = UpdateCEOFeedBackForm(request.POST)      
                if form.is_valid():  
                    obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                    obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                    obj.rm_rating = form.cleaned_data.get('rating')
                    obj.rm_feedback = form.cleaned_data.get('feedback')
                    obj.ceo_rating = form.cleaned_data.get('rating')
                    obj.ceo_feedback = form.cleaned_data.get('feedback')
                    obj.ceo_approved = form.cleaned_data.get('ceo_approved')
                    obj.report_manager = request.user
                    obj.ceo = request.user
                    obj.save()
                    return render(request, 'employee/updatefeedback.html', {'msg': "Updated your feedback!",})
        else:
            if request.method == 'GET':
                if result.ceo == request.user:
                    form = UpdateCEOFeedBackForm(initial = {'rating':result.ceo_rating,'feedback':result.ceo_feedback,'ceo_approved':result.ceo_approved})
                else:
                    form = UpdateCEOFeedBackForm()
            else:
                form = UpdateCEOFeedBackForm(request.POST)  
                # import pdb;pdb.set_trace()    
                if form.is_valid():  
                    obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                    obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                    obj.ceo_rating = form.cleaned_data.get('rating')
                    obj.ceo_feedback = form.cleaned_data.get('feedback')
                    obj.ceo_approved = form.cleaned_data.get('ceo_approved')
                    obj.ceo = request.user
                    obj.save()
                    return render(request, 'employee/updatefeedback.html', {'msg': "Updated your feedback!",})

    elif request.user.role == 'hr and rm':
        rm = ReportingManager.objects.get(employee=emp_id)
        if request.user == rm.report_manager:
            if request.method == 'GET':
                form = UpdateHRFeedBackForm(initial = {'rating':result.rm_rating,'feedback':result.rm_feedback,'hr_approved':result.hr_approved})
            else:
                form = UpdateHRFeedBackForm(request.POST)       
                if form.is_valid():  
                    obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                    obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                    obj.rm_rating = form.cleaned_data.get('rating')
                    obj.rm_feedback = form.cleaned_data.get('feedback')
                    obj.hr_rating = form.cleaned_data.get('rating')
                    obj.hr_feedback = form.cleaned_data.get('feedback')
                    obj.hr_approved = form.cleaned_data.get('hr_approved')
                    obj.report_manager = request.user
                    obj.hr = request.user
                    obj.save()
                    return render(request, 'employee/updatefeedback.html', {'msg': "Updated your feedback!",})
        else:
            if request.method == 'GET':
                if result.hr == request.user:
                    form = UpdateHRFeedBackForm(initial = {'rating':result.hr_rating,'feedback':result.hr_feedback,'hr_approved':result.hr_approved})
                else:
                    form = UpdateHRFeedBackForm()
            else:
                form = UpdateHRFeedBackForm(request.POST)
                # import pdb;pdb.set_trace()        
                if form.is_valid():  
                    obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                    obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                    obj.hr_rating = form.cleaned_data.get('rating')
                    obj.hr_feedback = form.cleaned_data.get('feedback')
                    obj.hr_approved = form.cleaned_data.get('hr_approved')
                    obj.hr = request.user
                    obj.save()
                    return render(request, 'employee/updatefeedback.html', {'msg': "Updated your feedback!",})  

    elif request.user.role == 'hr':
        if request.method == 'GET':
            if result.hr == request.user:
                form = UpdateHRFeedBackForm(initial = {'rating':result.rm_rating,'feedback':result.rm_feedback,'hr_approved':result.hr_approved})
            else:
                form = UpdateHRFeedBackForm()
        else:
            form = UpdateHRFeedBackForm(request.POST)       
            if form.is_valid():  
                obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                obj.hr_rating = form.cleaned_data.get('rating')
                obj.hr_feedback = form.cleaned_data.get('feedback')
                obj.hr_approved = form.cleaned_data.get('hr_approved')
                obj.hr = request.user
                obj.save()
                return render(request, 'employee/updatefeedback.html', {'msg': "Updated your feedback!",})  

    elif request.user.role == 'ceo':
        # import pdb;pdb.set_trace()
        if request.method == 'GET':
            if result.ceo == request.user:
                form = UpdateCEOFeedBackForm(initial = {'rating':result.ceo_rating,'feedback':result.ceo_feedback,'ceo_approved':result.ceo_approved})
            else:
                form = UpdateCEOFeedBackForm()
        else:
            form = UpdateCEOFeedBackForm(request.POST)
            if form.is_valid():  
                obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                obj.ceo_rating = form.cleaned_data.get('rating')
                obj.ceo_feedback = form.cleaned_data.get('feedback')
                obj.ceo_approved = form.cleaned_data.get('ceo_approved')
                obj.ceo = request.user
                obj.save()
                return render(request, 'employee/updatefeedback.html', {'msg': "Updated your feedback!",})

    elif request.user.role == 'rm':     
        if request.method == 'GET':
            if result.report_manager == request.user:
                form = UpdateFeedBackForm(initial = {'rating':result.rm_rating,'feedback':result.rm_feedback,})
            else:
                form = UpdateFeedBackForm()
        else:
            form = UpdateFeedBackForm(request.POST)     
            if form.is_valid():  
                obj = EmployeeFeedBack.objects.get(employee=emp_id,added_date__year=today.year)
                obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
                obj.rm_rating = form.cleaned_data.get('rating')
                obj.rm_feedback = form.cleaned_data.get('feedback')
                obj.report_manager = request.user
                obj.save()
                return render(request, 'employee/updatefeedback.html', {'msg': "Updated your feedback!",})

    return render(request, 'employee/updatefeedback.html', {'form': form, 'emp_id':emp_id, 'name':name,})

@login_required(login_url='/login/')
def form_status(request, emp_id):

    empset = EmployeeProfile.objects.exclude(Q(role='ceo')|Q(role='ceo and rm')|Q(id=request.user.id)|Q(emp_name__icontains='admin'))
    defaulters = []
    for emp in empset:
        reviewers = Reviewer.objects.filter(employee=emp.id,is_rm_approved=True, is_hr_approved=True)
        non_defaulters = CategoryResult.objects.filter(employee=emp.id).values_list('filled_by',flat=True)
        temp_list = Reviewer.objects.filter(employee=emp.id,is_rm_approved=True, is_hr_approved=True).exclude(reviewer_id__in=non_defaulters).values_list('reviewer', flat=True)
        for item in temp_list:
            defaulters.append(item)
    # import pdb;pdb.set_trace()
    s_defaulters = set(defaulters)
    l_defaulters = list(s_defaulters)
    defaulter_set = EmployeeProfile.objects.filter(id__in=l_defaulters)
    return render(request, 'employee/formstatus.html', {'defaulter_set': defaulter_set, 'emp_id':emp_id,})

@login_required(login_url='/login/')
def form_status_next(request, emp_id):
    # import pdb;pdb.set_trace()
    empset = Reviewer.objects.filter(reviewer=emp_id,is_hr_approved=True,is_rm_approved=True)
    emplist = []
    for emp in empset:
        resultset = CategoryResult.objects.filter(employee=emp.employee.id, filled_by=emp.reviewer.id)
        if not resultset.first():
            emplist.append(emp.employee)
    return render(request, 'employee/formstatusnext.html', {'emplist': emplist, 'emp_id':emp_id,})





# @login_required(login_url='/login/')
# def provide_feedback(request, emp_id):
#   name = EmployeeProfile.objects.only('emp_name').get(pk=emp_id).emp_name

#   # if request.method == 'GET':
#   #   # import pdb;pdb.set_trace()
#   #   form = FeedBackForm(initial = {'employee':name})
#   # else:
#   #   form = FeedBackForm(request.POST)       
#   #   if form.is_valid():  
#   #       obj = FeedBack()
#   #       obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id)
#   #       obj.filled_by = request.user
#   #       obj.rating = form.cleaned_data.get('rating')
#   #       obj.feedback = form.cleaned_data.get('feedback')
#   #       try:
#   #           obj.save()
#   #       except:
#   #           return render(request, 'employee/feedbackform.html', {'msg': "You have already given this feedback!",})
#   #       return render(request, 'employee/feedbackform.html', {'msg': "done",})


#   if request.user.role == 'HR':
#       if request.method == 'GET':
#           # import pdb;pdb.set_trace()
#           form = HRFeedBackForm()
#       else:
#           form = HRFeedBackForm(request.POST)     
#           if form.is_valid():  
#               obj = EmployeeFeedBack.objects.get(employee=emp_id) 
#               obj.hr_rating = form.cleaned_data.get('rating')
#               obj.hr_feedback = form.cleaned_data.get('feedback')
#               obj.hr_approved = form.cleaned_data.get('hr_approved')
#               try:
#                   obj.save()
#               except:
#                   return render(request, 'employee/feedbackform.html', {'msg': "You have already given this feedback!",})
#               return render(request, 'employee/feedbackform.html', {'msg': "done",})

#   elif request.user.role == 'CEO':
#       if request.method == 'GET':
#           # import pdb;pdb.set_trace()
#           form = CEOFeedBackForm()
#       else:
#           form = CEOFeedBackForm(request.POST)        
#           if form.is_valid():  
#               obj = EmployeeFeedBack.objects.get(employee=emp_id) 
#               obj.ceo_rating = form.cleaned_data.get('rating')
#               obj.ceo_feedback = form.cleaned_data.get('feedback')
#               obj.ceo_approved = form.cleaned_data.get('hr_approved')
#               try:
#                   obj.save()
#               except:
#                   return render(request, 'employee/feedbackform.html', {'msg': "You have already given this feedback!",})
#               return render(request, 'employee/feedbackform.html', {'msg': "done",})

#   else :
#       if request.method == 'GET':
#           form = FeedBackForm()
#       else:
#           form = FeedBackForm(request.POST)       
#           if form.is_valid():  
#               obj = EmployeeFeedBack()
#               obj.employee = get_object_or_404(EmployeeProfile, pk=emp_id) 
#               obj.rm_rating = form.cleaned_data.get('rating')
#               obj.rm_feedback = form.cleaned_data.get('feedback')
#               try:
#                   obj.save()
#               except:
#                   return render(request, 'employee/feedbackform.html', {'msg': "You have already given this feedback!",})
#               return render(request, 'employee/feedbackform.html', {'msg': "done",})

#   return render(request, 'employee/feedbackform.html', {'form': form, 'emp_id':emp_id, 'name':name,})

# 
# @login_required(login_url='/login/')
# def view_category_form(request, emp_id):

#   category_result_objs = CategoryResult.objects.filter(employee=emp_id)
#   response = {}
#   for category_result_obj in category_result_objs:
#       if response.has_key(category_result_obj.category):
#           data = response.get(category_result_obj.category)
#           data.append(
#               {
#                   'filled_by':category_result_obj.filled_by,
#                   'comments':category_result_obj.comments,
#               })
#       else:
#           response[category_result_obj.category] = [{
#                   'filled_by':category_result_obj.filled_by,
#                   'comments':category_result_obj.comments,
#               }] 
#   if category_result_objs.exists():
#       return render(request, 'employee/viewcategory.html', {'response':response})
#   return render(request, 'employee/viewcategory.html', {'msg':"Form not yet filled!"})

