# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-01 12:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0003_auto_20170228_0859'),
    ]

    operations = [
        migrations.CreateModel(
            name='CronInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('added_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('last_run_date', models.DateField()),
                ('processing_status', models.IntegerField(choices=[(0, b'idle'), (1, b'proccesing'), (2, b'completed'), (3, b'error')], default=0)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
