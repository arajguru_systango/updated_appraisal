from .base import *

NEWRELIC_ENV = "production"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'appraisal_system',
        'USER': 'arajguru',                     
        'PASSWORD': 'arastpl',
    }
}


STRIPE_PUBLIC_KEY = "pk_test_s40OE6vLgIYRtwfimYJkTBkA"
STRIPE_SECRET_KEY = "sk_test_IYNWFbFH0sMFYft3SMYX0L7U"

LOG_LEVEL = 'DEBUG' if DEBUG else 'INFO'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        'simple1' :{
         'format': '%(levelname)s %(asctime)s %(name)s.%(funcName)s:%(lineno)s- %(message)s'
         },
    },
    'handlers': {
        'file': {
            'level': LOG_LEVEL,
            'class':'logging.handlers.RotatingFileHandler',
            'filename': 'mysite.log',
            'formatter': 'verbose',
            'maxBytes': 1024*1024*5, # 5 MB
            'backupCount': 5,
        },
        'SysLog':{
          'level':'DEBUG',
          'class':'logging.handlers.SysLogHandler',
          'formatter': 'simple1',
          'address':('logs5.papertrailapp.com', 39631)
        }
    },
    'loggers': {
        'django': {
             'handlers':['file','SysLog'],
             'propagate': True,
             'propagate': True,
             'level':'INFO',
         },
        'django.request': {
             'handlers':['file','SysLog'],
             'propagate': True,
             'level':'ERROR',
         },
        'employee': {
            'handlers': ['file','SysLog'],
            'level': 'INFO',
        },
    }
}