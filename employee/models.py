#!/usr/bin/env python

# Python imports.
from __future__ import unicode_literals
import logging
import datetime
import os

# Django imports.
from django.contrib.auth.models import(
    BaseUserManager, AbstractBaseUser, PermissionsMixin)
from django.db import models
from django.utils import timezone
from django.contrib.auth.hashers import is_password_usable 
from django.core.exceptions import ValidationError
# Third Party Library imports


# local imports.
from configuration.models import *
from appraisal_system import constants

logger = logging.getLogger(__name__)


class MyUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        # username = self.model.normalize_username(username)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class EmployeeProfile(AbstractBaseUser, PermissionsMixin):
    emp_name = models.CharField(max_length=255, null=True,blank=True,verbose_name='Employee Name')
    emp_code = models.CharField(max_length=10, unique=True, blank=True,verbose_name='Employee Code')
    email = models.EmailField("Email", unique=True)
    password = models.CharField(max_length=100)
    role = models.CharField(max_length=40,choices=constants.ROLES,verbose_name='Role', null=True,blank=True)
    emp_level = models.ForeignKey(Level, verbose_name='Level', null=True,blank=True)
    emp_designation = models.ForeignKey(
        Designation, verbose_name='Designation', null=True,blank=True)
    emp_department = models.ForeignKey(
        Department, verbose_name='Department', null=True,blank=True)
    emp_stack = models.ForeignKey(Stack, verbose_name='Stack', null=True,blank=True)
    is_first_login = models.BooleanField('is first login', default=True)
    is_staff = models.BooleanField(
        'staff status',
        default=False,
        help_text=('Designates whether the user can log into this site.'),
    )
    is_active = models.BooleanField(
        'active',
        default=True,
        help_text=(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    USERNAME_FIELD = 'email'
    objects = MyUserManager()

    def __str__(self):
        return self.emp_name

    def get_full_name(self):
        return self.emp_name

    def get_short_name(self):
        return self.emp_name

    def __unicode__(self):
        return self.emp_name or u''

    class Meta:
        verbose_name_plural = 'Employee Profiles'
        verbose_name = 'Employee Profile'
        ordering = ['emp_name']

class Reviewer(TimeStampedModel):
    employee = models.ForeignKey(
        EmployeeProfile, related_name='employee')
    reviewer = models.ForeignKey(
        EmployeeProfile, related_name='reviewer')
    is_rm_approved = models.BooleanField(default=False, verbose_name='Is Feedback Manager Apporved')
    is_hr_approved = models.BooleanField(default=False)

    class Meta:
        unique_together = ('employee','reviewer')
    def __str__(self):
        return self.reviewer.emp_name
        
class ReportingManager(TimeStampedModel):
    employee = models.ForeignKey(
        EmployeeProfile, related_name='rep_manager_parent', unique=True)
    report_manager = models.ForeignKey(
        EmployeeProfile, on_delete=models.CASCADE, limit_choices_to={'is_staff' : True},verbose_name='Reporting Manager')
    class Meta:
        verbose_name_plural = 'Reporting Managers'
        verbose_name = 'Reporting Manager'

    def __str__(self):
        return self.report_manager.emp_name
 
    def clean(self):
        super(ReportingManager, self).clean()
        if not (self.employee_id and self.report_manager_id):
            raise ValidationError("Please select any value for fields") 
        elif  (self.employee_id == self.report_manager_id):
            raise ValidationError("Employee and ReportingManager cannot be the same!")

class CategoryResult(TimeStampedModel):
    employee = models.ForeignKey(EmployeeProfile, verbose_name='employee')
    category = models.ForeignKey(Category, related_name='category_result')
    comments = models.TextField(blank=True, null=True)
    filled_by = models.ForeignKey(
        EmployeeProfile, related_name='category_filled_by') 
    class Meta:
        unique_together=('employee','category', 'filled_by')


class EmployeeFeedBack(TimeStampedModel):
    employee = models.OneToOneField(EmployeeProfile, verbose_name='employee')
    rm_rating = models.FloatField(default=0.0, null=True,blank=True)
    hr_rating = models.FloatField(default=0.0, null=True,blank=True)
    ceo_rating = models.FloatField(default=0.0, null=True,blank=True)
    rm_feedback = models.TextField(null=True,blank=True)
    hr_feedback = models.TextField(null=True,blank=True)
    ceo_feedback = models.TextField(null=True,blank=True)
    report_manager = models.ForeignKey(EmployeeProfile, related_name='report_manager',null=True,blank=True)
    hr = models.ForeignKey(EmployeeProfile, related_name='hr',null=True,blank=True)
    ceo = models.ForeignKey(EmployeeProfile,related_name='ceo',null=True,blank=True)
    hr_approved = models.BooleanField(default=False)
    ceo_approved = models.BooleanField(default=False)
