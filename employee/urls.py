from django.conf.urls import url
from . import views

app_name = 'employee'

urlpatterns = [

		url(r'^$', views.home, name = 'home'),

		url(r'^save_employee_by_csv/$', views.save_employee_by_csv, name = 'save_employee_by_csv'),

		url(r'^save_kpi_by_csv/$', views.save_kpi_by_csv, name = 'save_kpi_by_csv'),

		url(r'^save_rm_by_csv/$', views.save_rm_by_csv, name = 'save_rm_by_csv'),

		url(r'^save_assignkpi_by_csv/$', views.save_assignkpi_by_csv, name = 'save_assignkpi_by_csv'),

		url(r'^user_home/$', views.user_home, name = 'user_home'),

		url(r'^report_manager_home/$', views.report_manager_home, name = 'report_manager_home'),

		url(r'^hr_home/$', views.hr_home, name = 'hr_home'),
		
		url(r'^ceo_home/$', views.ceo_home, name = 'ceo_home'),

		url(r'^view_profile/$', views.view_profile, name = 'view_profile'),

		url(r'^team/(?P<emp_id>[0-9]+)/$', views.team, name='team'),

		url(r'^define_reviewers/(?P<emp_id>[0-9]+)/$', views.define_reviewers, name='define_reviewers'),

		url(r'^view_reviewers/(?P<emp_id>[0-9]+)/$', views.view_reviewers, name='view_reviewers'),

		url(r'^delete_reviewer/(?P<reviewer_id>[0-9]+)/$', views.delete_reviewer, name='delete_reviewer'),

		# url(r'^fill_kpi_form/(?P<emp_id>[0-9]+)/$', views.fill_kpi_form, name= 'fill_kpi_form'),

		# url(r'^fill_category_form/(?P<emp_id>[0-9]+)/$', views.fill_category_form, name= 'fill_category_form'),

		# url(r'^update_category_form/(?P<emp_id>[0-9]+)/$', views.update_category_form, name= 'update_category_form'),

		url(r'^save_category_form/(?P<emp_id>[0-9]+)/$', views.save_category_form, name= 'save_category_form'),

		url(r'^save_category_form_new/(?P<emp_id>[0-9]+)/$', views.save_category_form_new, name= 'save_category_form_new'),

		url(r'^employees_under_rm/(?P<emp_id>[0-9]+)/$', views.employees_under_rm, name= 'employees_under_rm'),

		url(r'^view_all_employees/(?P<emp_id>[0-9]+)/$', views.view_all_employees, name= 'view_all_employees'),

		url(r'^form_status/(?P<emp_id>[0-9]+)/$', views.form_status, name= 'form_status'),

		url(r'^form_status_next/(?P<emp_id>[0-9]+)/$', views.form_status_next, name= 'form_status_next'),
		# url(r'^view_category_form/(?P<emp_id>[0-9]+)/$', views.view_category_form, name= 'view_category_form'),

		url(r'^view_competency/(?P<emp_id>[0-9]+)/$', views.view_competency, name= 'view_competency'),

		url(r'^view_competency_result/(?P<emp_id>[0-9]+)/(?P<category_id>[0-9]+)/$', views.view_competency_result, name= 'view_competency_result'),

		url(r'^view_competency_result_previous/(?P<emp_id>[0-9]+)/(?P<category_id>[0-9]+)/$', views.view_competency_result_previous, name= 'view_competency_result_previous'),

		# url(r'^view_kpi_form/(?P<emp_id>[0-9]+)/$', views.view_kpi_form, name= 'view_kpi_form'),

		# url(r'^provide_feedback/(?P<emp_id>[0-9]+)/$', views.provide_feedback, name= 'provide_feedback'),

		url(r'^update_feedback/(?P<emp_id>[0-9]+)/$', views.update_feedback, name= 'update_feedback'),

		url(r'^view_feedback/(?P<emp_id>[0-9]+)/$', views.view_feedback, name= 'view_feedback'),

		# url(r'^team/$', views.team, name= 'team'),
		url(r'^get_rating/$', views.get_rating, name='get_rating'),

]
