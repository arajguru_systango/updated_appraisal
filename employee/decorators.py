from django.http import HttpResponseRedirect
import datetime
from configuration.models import Schedule
from django.shortcuts import render
from employee.models import *

import logging

logger = logging.getLogger("employee")
# def pass_decor(value):
#   def check_render_and_disable_link_page(function):
#       def wrap(request, *args, **kwargs):
#           if value == False:
#               return render(request, 'employee/disabled.html')
#           # if link_is_disable:
#           # redirect to the link disable html page
#           # return HttpResponseRedirect()
#       return wrap 

#       wrap.__doc__=function.__doc__
#       wrap.__name__=function.__name__
#   return check_render_and_disable_link_page

from appraisal_system.utils import get_disable_date
def pass_decor(value):
    def decorator(func):
        def method(self, *args, **kwargs):
            logger.info('This is a test INFO error')
            # import pdb;pdb.set_trace()
            emp_id = kwargs.get('emp_id', None)
            # result = True
            result = get_disable_date(value,self.user, emp_id)
            if result == True or self.user.email in ('nidhir@systango.com','vinita@systango.com'):
                if value == 'view_feedback':
                    return render(self, 'employee/disabled.html')
                else:
                    return func(self, *args, **kwargs)
            else:
                # return func(self, *args, **kwargs)
                return render(self, 'employee/disabled.html')
        return method
    return decorator

def restrict_user(value):
    def decorator(func):
        def method(self, *args, **kwargs):

            emp_id = kwargs.get('emp_id', None)
            try:
                rmobj = ReportingManager.objects.get(employee=emp_id)
            except:
                rmobj = ""
            reviewers = Reviewer.objects.filter(employee_id=emp_id,is_rm_approved=True, is_hr_approved=True).values_list('reviewer_id', flat=True)
            logger.info('Inside decorator emp_id: %s current_user: %s rmobj: %s reviewers: %s'%(emp_id, self.user.id, rmobj, reviewers))
            
            if value == 'save_category_form':
                # if self.user.role == 'rm' and self.user == rmobj.report_manager or self.user == EmployeeProfile.objects.get(id=emp_id) or self.user.role == 'ceo':
                if self.user == EmployeeProfile.objects.get(id=emp_id) or self.user.role == 'ceo':
                    logger.info('Inside first if cond emp_id: %s current_user: %s rmobj: %s reviewers: %s'%(emp_id, self.user.id, rmobj, reviewers))
                    return func(self, *args, **kwargs)
                elif self.user.id in reviewers:
                    logger.info('Inside second if cond emp_id: %s current_user: %s rmobj: %s reviewers: %s'%(emp_id, self.user.id, rmobj, reviewers))
                    return func(self, *args, **kwargs)
                else:
                    return render(self, 'employee/unauthorized.html')
            if value == 'team' or value == 'employees_under_rm':
                if self.user == EmployeeProfile.objects.get(id=emp_id):
                    logger.info('Inside third if cond emp_id: %s current_user: %s rmobj: %s reviewers: %s'%(emp_id, self.user.id, rmobj, reviewers))
                    return func(self, *args, **kwargs)
                else:
                    return render(self, 'employee/unauthorized.html')
            if value == 'view_competency' or value == 'view_competency_result' or value == 'update_feedback':
                # if self.user.role == 'rm' and self.user == rmobj.report_manager or self.user.role == 'hr' or self.user.role == 'ceo':
                if self.user.role == 'rm' or self.user.role == 'hr' or self.user.role == 'ceo'or self.user.role == 'hr and rm' or self.user.role == "ceo and rm" :
                    logger.info('Inside fourth if cond emp_id: %s current_user: %s rmobj: %s reviewers: %s'%(emp_id, self.user.id, rmobj, reviewers))
                    return func(self, *args, **kwargs)
                

                else :
                    return render(self, 'employee/unauthorized.html')
        return method
    return decorator
