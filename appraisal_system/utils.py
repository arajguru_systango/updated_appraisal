from datetime import date
from django.core.mail import send_mail, BadHeaderError,EmailMessage
from django.conf import settings
from django.template.loader import render_to_string
from employee.models import *
from configuration.models import *
import kronos
import datetime
from django.contrib.auth.models import User

def send_email_appraisal_portal_active():
    subject = "Appraisal Portal Active"
    schedule = Schedule.objects.first()
    last_date_to_fill = schedule.start_date + datetime.timedelta(days=2)
    last_date = schedule.start_date + datetime.timedelta(days=3)
    context = {
        'last_date_to_fill':last_date_to_fill,
        'last_date' : last_date
        }
    message = render_to_string('email/portal_active.html',context).strip()
    # user_list = ['systangoteam@isystango.com',]
    user_list = EmployeeProfile.objects.all().values_list('email', flat=True)
    email = EmailMessage(subject,message,settings.EMAIL_HOST_USER,user_list)
    try:
        email.send(fail_silently=False)
    except Exception, e:
        pass

def send_email_reminder_mail():
    subject = "Reminder Mail"
    schedule = Schedule.objects.first()
    last_date = schedule.start_date + datetime.timedelta(days=7)
    context = {
        'last_date':last_date,
        }
    message = render_to_string('email/reminder.html',context).strip()
    user_list = EmployeeProfile.objects.all().values_list('email', flat=True)
    # user_list = ['systangoteam@isystango.com',]
    email = EmailMessage(subject,message,settings.EMAIL_HOST_USER,user_list)
    try:
        email.send(fail_silently=False)
    except Exception, e:
        pass

def send_email_employee_finalize_reviewer(reviewerset, rm_mail):
    subject = "Reviewers Defined"
    schedule = Schedule.objects.first()
    last_date = schedule.start_date + datetime.timedelta(days=5)
    if reviewerset:
        context = {
            'name':reviewerset[0].employee.emp_name,
            'reviewerset': reviewerset,
            'last_date' : last_date,
        }
        message = render_to_string('email/employee_defined_reviewers.html',context).strip()
        user_list = [rm_mail,]
        email = EmailMessage(subject,message,settings.EMAIL_HOST_USER,user_list)
        try:
            email.send(fail_silently=False)
        except Exception, e:
            pass
    return 

def send_email_hr_finalize_team():
    subject = "Fill Up Team Appraisal"
    schedule = Schedule.objects.first()
    last_date = schedule.start_date + datetime.timedelta(days=8)
    context = {
        'last_date':last_date,
        }
    message = render_to_string('email/reminder_hr_finalize_team.html',context).strip()
    user_list = EmployeeProfile.objects.filter(role='hr').values_list('email', flat=True)
    email = EmailMessage(subject,message,settings.EMAIL_HOST_USER,user_list)
    try:
        email.send(fail_silently=False)
    except Exception, e:
        pass


def send_email_reviewer_fill_feedback(teamset, emp_mail):
    subject = "Fill Up Team Appraisal"
    schedule = Schedule.objects.first()
    last_date_to_fill = schedule.start_date + datetime.timedelta(days=14)
    last_date = schedule.start_date + datetime.timedelta(days=15)
    if teamset:
        context = {
            'teamset': teamset,
            'last_date_to_fill' : last_date_to_fill,
            'last_date':last_date
        }
        message = render_to_string('email/reviewer_fill_feedback.html',context).strip()
        user_list = [emp_mail,]
        email = EmailMessage(subject,message,settings.EMAIL_HOST_USER,user_list)
        try:
            email.send(fail_silently=False)
        except Exception, e:
            pass
    return


def send_email_rm_fill_feedback(empset, rm_mail):
    subject = "Fill Up Feedback"
    schedule = Schedule.objects.first()
    last_date_to_fill = schedule.start_date + datetime.timedelta(days=36)
    last_date = schedule.start_date + datetime.timedelta(days=37)
    if empset:
        context = {
            'empset': empset,
            'last_date_to_fill':last_date_to_fill,
            'last_date' : last_date
        }
        message = render_to_string('email/rm_fill_feedback.html',context).strip()
        user_list = [rm_mail,]
        email = EmailMessage(subject,message,settings.EMAIL_HOST_USER,user_list)
        try:
            email.send(fail_silently=False)
        except Exception, e:
            pass
    return

def send_email_feedback_to_employee(feedback, emp_mail):
    subject = "View Your Feedback"
    if feedback:
        context = {
            'feedback': feedback,
        }
        message = render_to_string('email/employee_feedback.html',context).strip()
        user_list = [emp_mail,]
        email = EmailMessage(subject,message,settings.EMAIL_HOST_USER,user_list)
        try:
            email.send(fail_silently=False)
        except Exception, e:
            pass    
    return

def send_email_cron_status(status):
    subject = "Cron Status"
    if status == 0:
        msg = "Please configure the start date. Cron will not run until start date is set."    
    elif status == 1:
        msg = "Cron is running"
    elif status == 3:
        msg = "Cron errored out"
    context = {
        'msg':msg,
        }
    message = render_to_string('email/cron_status.html',context).strip()
    user_list = ['pjaiswal@isystango.com',]
    email = EmailMessage(subject,message,settings.EMAIL_HOST_USER,user_list)
    try:
        email.send(fail_silently=False)
    except Exception, e:
        pass

def get_disable_date(func,user, emp_id=None):

    schedule = Schedule.objects.first()
    today = date.today()
    from datetime import datetime
    start_time = datetime(2018, 4, 24 , 12, 30) 
    now = datetime.now()
    import datetime
    start_date = schedule.start_date
    if func == 'define_reviewers' or func == 'view_reviewers':
        if today >= start_date and today <= start_date + datetime.timedelta(days=2) :
            return True
        elif (user.email == "anmol@systango.com" and today <= start_date +datetime.timedelta(days=7)):
            return True
        else:
            return False
    if func == 'save_category_form' :
        objset = Reviewer.objects.filter(employee=emp_id, reviewer=user.id)
        # import pdb;pdb.set_trace()
        if today >= start_date and today <= start_date + datetime.timedelta(days=2) :
            return True
        elif (user.email == "anmol@systango.com" and today <= start_date +datetime.timedelta(days=7)):
            return True
        elif user.id != emp_id and objset.exists():
            if today >= start_date +datetime.timedelta(days=8) and today < start_date + datetime.timedelta(days=21):
                return True
        else:
            return False
    if func == 'team_view' :
        if today >= start_date + datetime.timedelta(days=8) and today < start_date + datetime.timedelta(days=21):
            return True
        else:
            return False
    if func == 'view_competency':
        if user.role == 'rm' :
            if today >= start_date +datetime.timedelta(days=21) and today < start_date + datetime.timedelta(days=41):
                return True
            else:
                return False
        else:
            return True
    if func == 'view_feedback':
        return False


