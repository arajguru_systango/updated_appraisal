# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-14 13:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0007_auto_20170314_1042'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employeeprofile',
            name='role',
            field=models.CharField(blank=True, choices=[(b'employee', b'Employee'), (b'hr', b'HR'), (b'ceo', b'CEO')], max_length=40, null=True, verbose_name='Role'),
        ),
    ]
